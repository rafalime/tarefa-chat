/* Funcionalidade 1: Adicionar nova mensagem
    1) Pegar valor do campo input
    2) Criar elemento
    3) Adicionar valor do input
    4) Adicionar id único para posteriormente deletar */

// Vars
let idx = 1

// Consts
const input = document.querySelector("#input")
const addbtn = document.querySelector(".send")
const chat = document.querySelector(".main")

function addNewMsg() {
    // Pick value in input field
    let msg = input.value
    if (msg == '') {
        alert("Por favor digite uma mensagem")
    } else {
        // Create elements
        // First div
        let msgbody = document.createElement("div")
        msgbody.setAttribute("id", idx)

        msgbody.classList.add("msg")
        msgbody.innerHTML = `<p id='${idx}'>${msg}</p>`
        // Second div --> Buttons
        let buttons = document.createElement("div")
        buttons.classList.add("btns")
        buttons.setAttribute("id", idx)
        buttons.innerHTML = `<button class='edit' id='${idx}' onclick='editMsg(${idx})'>Editar</button><button class='del' id='${idx}' onclick='removeMsg(${idx})'>Excluir</button>`
        idx++
        // Add to the chat
        chat.append(msgbody)
        chat.append(buttons)

        // Clear input
        input.value = ''
    }
}
// Envent listener for this function
addbtn.addEventListener("click", addNewMsg)

/* Funcionalidade 2: Função de remover uma mensagem do chat
    As mensagens tem um id único, ao clicar no botão excluir ele deve excluir a mensagem com esse id
    */

function removeMsg(id) {
    // Pegar todos os elementos com esse id e remove-los
    // console.log("Função acionada")
    // let msg = document.querySelector(`#${id}`)
    let sure = confirm("Quer mesmo apagar essa mensagem permanentemente?")
    if (sure){
        for (let con = 1; con < 3; con++) {
            let msg = document.getElementById(id)
            msg.remove()
        }
    }
    

}

/* Funcionalidade 3: Função de editar uma mensagem do chat
    Cada mensagem tem um id, logo ao clicar no botão de mesmo id será aberto uma caixa de diálogo onde o usuário irá informar a nova mensagem */
function editMsg(id) {
    // Pegar a mensagem atual
    let now = document.getElementById(id)
    // console.log(now)
    now.innerText = prompt("Edite sua mensagem", now.innerText)
}